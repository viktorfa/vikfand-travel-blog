---
title: "I Vietnam"
date: 2018-10-27T07:07:30+02:00
draft: false
type: post
---

# I Vietnam

- 10:20 buss drar fra Nanning
- 11:20 kort pause ved rasteplass
- 13:30 ankommer grensestasjon i Kina
- 14:20 buss drar fra grense i Vietnam
- 15:45 liten lunsjpause ved rasteplass i Vietnam
- 18:00 ankommer Hanoi
- 21:20 spiser kyllingsuppe på gata

(kinesisk tid)

![
Kart over bussreisen fra Nanning, Kina til Hanoi, Vietnam](https://lh3.googleusercontent.com/2s8C3MNkPJpk4_mB9HP4kn67ycuICm67y39-CxXlQhQQ5QKAR5yFVFTLTPFnEtIT_INUW9ckIOqPF8QZeZp4L6QEIfWvmfRzcYHL0Q7guNxPyoQSaCCMT8LlS0ACrS5TPdMVUtGPLZSa3KOqrZyup-N9gMW0z0-v7g-6xJW7C3sPAncOp-WzRfzEE0HMGhzPnl_5SDqeVq4qWukWmfwj-K0wKodaGq9tZhspRh5lw3il78n5TJMHuXEp1NWl9iZaaiPkgwcv1sOycNe-n4QqCcqXEgzqYNaZ2c3p5H1_87lHePDgbpnjSn9qMJvl0q-MR8DOKIDHVFgXUfSd2wa--YstvEqp2yDFpUTbjwOJREkCpDtKzQaF6Z-8M7UY4auymqinqQcGZGrD9_wQxaM0qWXtLWFm1Xn8kZxsx_7PUaQ35Vzgv4P3-Z8KuBYdpCpwlGLWeHBSskOj56WJbZxLmIG2U_Cd_txdut7rSn5C4Reqo3fqaUSXKs--EHTaMLYpySI_rZw-1TWqAiMGWGdqBKDxoEnzLzJj1lyYbHJ_BQv5OmwGe8dPVYojde_HI_sxwYC75bZqOEBdM2CdO2jBQKnb3sQ2fUCi7TmpMM-MMD-Zz1hxNQyaGx3l8pXeVPnFvz775O8r5un7umEF4X1Tc1CaYel4qtIt_Ylc3UXCPPqmm0gdPj2-r8ElcMFUmjNfpZSdGXroCBAl3EpSahg=w1176-h734-no)
_Reisen på 40 mil tok ca 8 timer inkludert ca 1,5 timer med venting og pause_

Jeg klarte å kjøpe biletter til Vietnam helt selv. Kom fram til busstasjonen og sa jeg skulle til Yuenan (Vietnam). Hadde så vidt penger på den kinesiske kontoen til å betale. Jeg klarer meg faktisk greit i Kina fordi jeg kan litt av språket. Her i Vietnam er jeg litt mer hjelpesløs, men det veier opp med at flere snakker engelsk (i Hanoi, hvertfall). Bussturen gikk fortere enn planlagt, og alt gikk helt fint. Var faktisk bitte lite bekymra for da jeg skulle ut av Kina, for jeg hadde brukt 30-dagersvisumet for alt det var verdt. 30 dager fra _første dag etter ankomst_. Så jeg hadde sovet 30 netter i Kina, men hadde tilbrakt tid i Kina i 31 dager. Men travelchinaguide.com påsto at reglene var sånn, så jeg stolte på dem. Jeg måtte stå ukomfortabelt lenge foran 2 kameraer og 2 grensevakter ved kontrollen på kinesisk side. Mannen som behandlet meg var ung og hadde 3 dekorasjoner på uniformen sin. Han så mest på skjermen sin, men så opp på meg minst 10 ganger for å se om jeg var samme person som på bildene han hadde på skjermen. En dame med litt flere dekorasjoner sto ved siden av, og ga heldigvis oppmuntrende nikk til sin underordnede da han tasta inn litt av hvert. På vietnamesisk side var jeg også den i reisefølget som brukte lengst tid, da jeg hadde en litt spesielt "e-visa" som er nytt, og antakelig ikke veldig mye brukt ved akkurat den grenseovergangen. Han som behandlet meg var heldigvis en godt voksen mann med naturlig autoritet over seg, så han fikk ting gjort, og skrev noe med pennen sin for å gjøre opp for det manglende stempelet for e-visum.

![
Vi venta på grensestasjonen til en ny buss med vietnamesiske skilter plukka oss opp](https://lh3.googleusercontent.com/oXevSHScbjnoEY9Ei5dUTQoTWrmMhSsae86WgJZ8p-fLrdeWZQ3_AhVgh5JHqeUaL-l--o2HdV-Aj5aFr20Zi-RyIabwNJXk0xHC4jorgmlXMtHXQyA2EKESlWuSO8vT8lLV3IRctPW76854pLBeduChTL-hEdjYvb3CdH2dxVOgg56ANuVpi3M2ts3b0an7Dldp6FR1jO8xvnjfDZintY6tEn9U91jPRgekEGX5gvUmMNKTJaBmWK6oDJM8BBiZvuHyn2ec1XGlBEMENv2hxgHh9l1vJFNDeBcY9uXplR85J7zajRH94lqwJuLoJ2RlEX-js5pUSztQSjJF5koRKEPKF3bJP2h_1V6tIItG9C3gnYlxjAvGLrofEU1p-flsar-uU7mqKgkTo_x-svFZ9cbn3cX0CUfDTZ7uiYdTlgqX8eJSqJCcODfqlbD8j6Y1MwZdiUaCW4QQdngzU7erRHd5RQhgHbCHjXU-4mJwEWemZGo_QFclh1FwJZusAJ2GlxxPM8k_NXVIvDmvu_lKpNnvOZ0CkRvlOD7vAJZyFr5x5ID_08yBr-S1SfIvN16u_ynJm4t4YBA5khD6441TZzyudwGls1fN88SuXxsqyBh7reFux52d_dDwqvHS2-sFqfxSnWjbPaIrRmFeeMYgg44kPiJL9s-vZVyqLYDAj5uReE2u38aj4K6z5TvIDrmdOKFYlecHo_GLxI98Jxc=w1202-h901-no)
_Grensestasjonen på kinesisk side. Det var korte køer, men vi måtte vente en stund på en ny buss med vietnamesiske skilter for å komme oss videre._

Begge sidene av grensen hadde et vakkert karst landskap små, men dramatiske topper av stein og grønne planter stakk opp fra slettene med åkre av sukkerrør og ris. På kinesisk side kjørte vi på en motorvei for kun biler, men i Vietnam kjørte vi på _veien_. _Veien_ har to felt for tunge kjøretøy og brede skuldre for alt annet som vil si motorsykler, sykler, gående, og i ny og ned en vannbøffel. Det er artig med de små forskjellene mellom landene. Til og med den fattige Guangxi-provinsen i Kina har skutt forbi sin motpart i Vietnam med bedre infrastuktur, flere og bedre biler, og avansert grenseovergangsystem med kameraer og datamaskiner. Alt foregikk for hånd ved den vietnamesiske grensestasjonen. Det gjør Vietnam litt mer interessant for meg. Jeg visste f eks ikke at man kunne frakte både vannbøffel og geiter på en liten tilenger bakpå en moped. Her så jeg også bønder jobbe for hånd på risåkrene sammen med vannbøfler, haner og gjess. Jeg så lite sånt i Guangxi, men i Anhui lenger nord i landet var høstingen i gang da jeg var der, og folk holdt på for hånd der ute også.

![Karst landskap er på begge sidene av grensen mellom Vietnam og Kina](https://farm5.static.flickr.com/4731/24492842287_245571c398_b.jpg)
_Karst landskap med risåkre preger denne regionen. Bilde fra hiveminer.com_

Bussen stoppa litt utafor sentrum av Hanoi, så det var noen kilometer til hostellet mitt. Jeg ble med en gang spurt om jeg trengte taxi, og jeg sa instinktivt nei. Jeg hadde ikke internett her, så jeg var ganske hjelpesløs. Jeg gikk med ryggen rett i en helt tilfeldig retning helt til jeg så litt for lenge sidelengs inn i en kafe, der eieren sa noe til meg. Jeg svarte og spurte om de hadde wifi. Det hadde de så jeg satt meg ned. Jeg bestilte en ananassmootie til 40k dong eller 15 kr etter at eieren viste meg valget mellom ananas og mango. Der satt jeg og overhørte to kinesere som satt ved siden av mens jeg fant ut hvor jeg skulle, lasta ned Grab og Foodly, og bestilte en taxi. Jeg gjorde en dum tabbe da jeg gikk inn i feil taxi da jeg så en bil med nesten samme skiltnummer stoppe rett utafor. Jeg begynte å lure litt da sjåføren spurte hvor jeg skulle. Men etter å ha brukt Didi i Kina, så er jeg vant med å få sånne spørsmål selv om man skulle tro at man enkelt kunne se det i appen. Jeg innså at jeg gjorde en feil og fikk dårlig samvittighet da sjåføren fra Grab ringte meg flere ganger. Jeg bytta til flumodus og tenkte på noe annet. Den andre tabben var da sjåføren stoppa på riktig sted, og jeg sa "nei, det er ikke her". Jeg hadde forventa et skilt, og siden jeg var helt hjelpesløs uten internett, måtte jeg nesten ha bekreftelse før jeg gikk ut. Det endre med at sjåføren ringte hostellet, og resepsjonisten kom ut for å hente meg. Da gjorde jeg en mye større tabbe ved å glemme den lille sekken min med alle de viktige tingene i. Jeg merka det først da jeg ble spurt om passet mitt ved resepsjonen. Jeg håpa virkelig at sjåføren var en god person, noe jeg trodde han var, da resepsjonisten ringte ham tilbake. Takk og pris at vi hadde nummeret hans i det hele tatt. Han kom tilbake rimelig fort og ville ha 20k dong eller 6 kr ekstra. Jeg kunne gjerne gitt 10000kr for å få pc-en, passet og bankkortet tilbake der og da.

![
Smoothie fra fersk ananas kosta 40000 dong eller 15 kr](https://lh3.googleusercontent.com/0GN6OezqfMx8Bj51AIfNb_61ch4XF8KhCyEtY-S59qWnjjupKjktf4q6H9QY3i3MCrvVoHw458VGFfI8Xv1GIeW-iQfgG7LXX3PS3SZOy7NrG9VrjJCgAdbOcpTXG84029LZPIpwNzjdKbMUDrqX02isFA_9Xb1ABnqaAN-4wWHkcOUgMWeV5BLsO_1gLscHMpviqxqLIVNL7ExUxH05uKvPjimDarXzI9-xN-jQRlLBfOFPA3T3NhrxI8IDkmchtyayY0P5QbH9Xv6zniGgMXKFlnPScVpypsDr6HgNIBowxnVVxrvo17uoP6t6JPU1IHeWtippPIz7DtrBk0enTrQwM33qsl6qUsQdgePy-hi7BnM21mqcTcX6t-pkaRwtI9qE_J-zZxNohKXjsDrVr4Rz4xK-23h7GKp1Gmsn0vWwNshK2co5fW5BGBemhpOmHe3ls79-j80-ZepAMKgOmzszEGeFxS_MibuQ_elAueZcicNJddMxlxooxTWgGcZmisyXUlasOt4aU4W2p2GLpUz1s597rz49Nl9QbzJlmi2lJOSXwhBbMurAEvKjuvTOUASfkK7ceair09IJspxc_BJl4XRz6r-IDUXf0ruT9wo-JGQOPFidOn-L5cXzDLSYuX3weRWRE_g8KkXF7P3YSrbgvROqL4oo9jAWYzg8ZzI9oXex-H9e0YIUvDAJRyPZByG1NDrJkDQdXOcPuSE=w676-h901-no)
_Smoothie fra fersk, tropisk frukt og god kaffe gjør Vietnam ekstra attraktivt. Sånt er også betydelig billigere hvis man kjøper fra en gatebod._

Nå sitter jeg sammen med eieren av en liten kafe i Hanoi og drikker øl. Han røyker og ser på en film på Youtube. Jeg skriver det her og ser på bilder. Jeg gikk rundt i Hanoi og fikk litt samme følelse som man får på Khaosan Road i Bangkok. Det er veldig kult første gang, men det blir litt mye med så mange folk og de samme butikkene overalt. Det var skikkelig liv i gatene her da. En god miks av lokale, kinesiske turister, backpackere og eldre turister går rundt i gatene og beundrer de billige prisene og rare tingene som skjer her. Hadde egentlig ikke forventa at Hanoi hadde et sånt backpackerdistrikt, men det er minst like stort som det i Saigon, og kan sammenligens med Khaosan Road også.

<div>
  <video width="240" controls>
    <span>Yrende gateliv en fredag kveld i Hanoi</span>
    <source src="/videos/hanoi-renao.mp4"/>
  </video>
</div>
_Avslutter med en video fra gatelivet ved backpackerdistriktet i Hanoi. De stengte av veiene for motorkjøretøy denne fredagen, så både lokale og turister kunne kose seg litt._
