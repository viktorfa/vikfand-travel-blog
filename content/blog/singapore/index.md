---
title: Singapore
date: 2020-02-23
---

![Gardens at the Bay. Den kuleste parken i Singapore.](https://i.ytimg.com/vi/CyuOFWCOV_M/maxresdefault.jpg)
Gardens at the Bay. Den kuleste parken i Singapore.

Jeg hadde forventa at Singapore skulle være en stor by, og bare en stor by. Det er for så vidt sant at den er det, men i motsetning til mange andre byer, har de klart å ta ganske godt vare på byen sin her. Det er lite trafikk, veldig bra offentlig transport, rent og lite forurensing. Det er mange parker rundt omkring, og til og med ganske store områder langs kysten med strender og parker som grenser til å være litt jungel. Mest overraskende var at de har tatt ganske godt vare på den fine klassiske arkitekturen fra kolonitiden. Det er langt fra bare drabantbyblokker og glasskyskrapere her, men mange nabolag med slike gipsfasader man ser i bl.a. Amerika og Portugal. Ofte var disse byggene bygd over fortauet slik at de gir sky for regn og sol til fotgjengere, og var bare på to etasjer, som er litt rart da man skulle tro de ville utnytte høyden litt mer. Singapore og Hong Kong er på den måten veldig forskjellige.

Jeg bodde i Little India, den billigste, sentrale bydelen, der en seng på et herberge kan fås til litt over hundrelappen. Det er en fin bydel i seg selv, og veldig fin til å liksom være slummen. Hvis man leter kan man se litt søppel i gatene her, og man kan også se noen litt elendige folk squatte rundt omkring.

Omtrent alle hus er i kolonistil med 2 etasjer, som gir bydelen en sjarm. En annen sjarm er at Little _India_ ikke bare er et navn – det er faktisk stort sett bare indere som holder til her. Det lukter curry fra restauranter, damer går med sari og har rød prikk i panna, restauranter o.l. har Bollywood som tema.

![Little India.](https://live.staticflickr.com/4065/4525337533_929cd446e5_b.jpg)
Little India.

Singapore er en øy på tuppen av Malayhalvøya og var som Hong Kong bare en lite merkverdig fiskerlandsby før britene satt opp en handelspost der. Det var originalt muslimske malayer som bodde der, men stedet ble fort mer multikulturelt da kinesere og indere grep muligheten til å leve av handelen på den strategisk liggende kolonien. Dette var greit for britene, da handelen gikk bedre, og de dessuten hadde behov for arbeidere og tjenere fra India og Kina, da malayer ikke er kjente for å være flittige og hardtarbeidende. Idag er den etniske sammensetningen av Singapore kinesere, malayer, indere, og et drøss andre folkeslag eller blandinger. Disse folkegruppene liker å holde seg for seg selv, så selv om det forekommer, så er ekteskap mellom etnisitetene ganske sjeldent, og samfunnet er segregert i mye større grad enn i land som USA.

Den enorme indiske befolkningen ble brukt som billig arbeidskraft i hele det britiske imperiet, og idag spiller de denne rollen i mye av Asia. Mange av de tidlige indiske bosetterne har sikkert blitt rike innen nå, men det importeres fortsatt indere fra India til å gjøre lavtlønnet arbeid, som å stå med drillhammer langs veien under sola i 35 grader pluss. Og dette merker man litt i Little India. Selv om Singapore liksom skal være så dyrt, fikk jeg en hårklipp til bare 50kr, og man kan få et måltid til omtrent 40kr.

![Gardens at the Bay.](https://www.telegraph.co.uk/content/dam/Travel/2019/February/singapore-iStock-629183422.jpg?imwidth=1400)
Gardens at the Bay med "menneskelagde trær".

Jeg planla egentlig ingenting for reisen min til Singapore, så alt jeg gjorde var nesten bare å spontant gå forskjellige steder. Jeg så det tidligere fortet i Raffles Park, Sentosa-øya med strender, fyrverkeri og neonlys, gikk en tur i halvjungel i Southern Ridges, så de menneskelagde trærne og de tre skyskraperne med en plattform på toppen ved Gardens at the Bay. Dro ikke til noen museer, men tok en tur innom nasjonalbiblioteket og så en liten utstilling de hadde der. Det var forsåvidt et helt greit besøk, men man skulle nok helst tatt en drink i toppen av en skyskraper, og besøkt ArtScience Museum, for å få litt mer ut av byen.
